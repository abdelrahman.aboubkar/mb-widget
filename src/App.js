import logo from './logo.svg';
import './App.css';
import { useEffect } from 'react';

function App() {
  console.log(window.MessageBirdChatWidget)
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Here is an example of OCW
        </p>
        <button
          className="App-link"
          onClick={() => {
            const newWidgetId = "e1df2211-75f7-41d6-a4cb-ac52afe920d3";
            window.MessageBirdChatWidget.shutdown().then(() => {
              console.log("in shutdown");
              window.MessageBirdChatWidget.init( "e1df2211-75f7-41d6-a4cb-ac52afe920d3");
            });
          }}
        >
          Change widget
        </button>
      </header>
    </div>
  );
}

export default App;
